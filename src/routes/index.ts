import { IRoutes } from "./routes.types";
import customerRouter from "../routes/customer/customer.routes";
import swaggerUi from "swagger-ui-express";
import * as specs from "../common/swagger";
import { errorHandler } from "../common/middlewares";

const routes: IRoutes = {
  allRoutes: [{ path: "/customer", router: customerRouter.router }]
};

routes.init = (app: any) => {
  if (!app || !app.use) {
    console.error(
      "[Error] Route Initialization Failed: app / app.use is undefined"
    );
    return process.exit(1);
  }

  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs.default));

  routes.allRoutes.forEach(route => app.use(route.path, route.router));
  /* eslint-disable no-unused-vars */
  app.use("*", (request: any, response: any, next: any) => {
    const message = ["Cannot", request.method, request.originalUrl].join(" ");
    response.status(404).send(message);
  });
  /* eslint-enable no-unused-vars */
  app.use(errorHandler);
};

export { routes };
