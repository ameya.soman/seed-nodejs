import express from "express";
import customerService from "./customer.service";
import { ICustomer } from "./customer.types";
import { ResponseWrapper } from "../../common/response/responseWrapper";
import { customerLoginRequestValidator } from "./customer.validator";
import { validate } from "../../common/requestValidator/requestValidator";

const router = express.Router();

/**
 * @swagger
 *
 * /customer/login:
 *   post:
 *     description: Login to the application
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               customerId:
 *                 type: string
 *             example:   # Sample object
 *               customerId: 'asdfcvde'
 *     responses:
 *       200:
 *         description: login
 */
router.post(
  "/login",
  customerLoginRequestValidator,
  validate,
  async (req: any, res: any, next: any) => {
    try {
      const { customerId } = req.body;
      const result: ICustomer = await customerService.login(customerId);
      res.status(200).send(new ResponseWrapper(result));
    } catch (error) {
      next(error);
    }
  }
);

export default { router };
