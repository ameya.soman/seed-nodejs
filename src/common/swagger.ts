import swaggerJsdoc from "swagger-jsdoc";
const options = {
  definition: {
    openapi: "3.0.0", // Specification (optional, defaults to swagger: '2.0')
    info: {
      title: "Hello World", // Title (required)
      version: "1.0.0" // Version (required)
    }
  },
  // Path to the API docs
  apis: ["src/routes/*/*.routes.ts"]
};
const specs = swaggerJsdoc(options);
export default specs;
