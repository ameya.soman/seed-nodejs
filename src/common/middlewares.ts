/* eslint-disable no-unused-vars */
export const errorHandler = (
  error: any,
  request: any,
  response: any,
  next: any
) => {
  if (!error) {
    return;
  }
  if (error.status) {
    response.statusMessage = error.message;
    return response.status(error.status).send({
      error: {
        message: error.message,
        errorList: error.errorList
      }
    });
  }
  return response.status(500).send({
    error: {
      message: error.message,
      errorList: error
    }
  });
};
/* eslint-enable no-unused-vars */
